import tensorflow as tf
from absl import app, flags
from absl.flags import FLAGS
import core.utils as utils
from core.yolov4 import filter_boxes
from PIL import Image
import cv2
import numpy as np
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession
import time

USE_CUDA = True
flags.DEFINE_boolean('tiny', False, 'yolo or yolo-tiny')

flags.DEFINE_string('model', 'yolov4', 'yolov3 or yolov4')
flags.DEFINE_string('output', 'result.png', 'path to output image')
flags.DEFINE_string('framework', 'tf', '(tf, tflite, trt')
flags.DEFINE_string('weights', './checkpoints/yolov4-416.tflite','path to weights file')
flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_string('image', './data/kite.jpg', 'path to input image')
flags.DEFINE_float('iou', 0.45, 'iou threshold')
flags.DEFINE_float('score', 0.25, 'score threshold')


# 6) Dump and Inferenced frames
def show_image(original_image, pred_bbox):
    image = utils.draw_bbox(original_image, pred_bbox)
    image = Image.fromarray(image.astype(np.uint8))
    image.show()

# 5) Extract results
def extract_result(pred,input_size):
    boxes, pred_conf = filter_boxes(pred[0], pred[1], score_threshold=0.25, input_shape=tf.constant([input_size, input_size]))
    boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
        boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
        scores=tf.reshape(
            pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
        max_output_size_per_class=50,
        max_total_size=50,
        iou_threshold=FLAGS.iou,
        score_threshold=FLAGS.score
    )
    pred_bbox = [boxes.numpy(), scores.numpy(), classes.numpy(), valid_detections.numpy()]
    return pred_bbox

# 1) Load, Preprocess the image to required size and cast
def load_images(image_path,input_size):
    original_image = cv2.imread(image_path)
    original_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2RGB)

    image_data = cv2.resize(original_image, (input_size, input_size))
    image_data = image_data / 255.
    
    images_data = []
    for i in range(1):
        images_data.append(image_data)
    images_data = np.asarray(images_data).astype(np.float32)
    return images_data,original_image

#CUDA_VISIBLE_DEVICES=0 python3 test_fps.py
# 100 inferences required 130.84606337547302, so 1.3084557437896729 FPS 
def main(_argv):
    numb_infs = 100
    config = ConfigProto()

    if USE_CUDA:
        physical_devices = tf.config.experimental.list_physical_devices('GPU')
        config.gpu_options.allow_growth = True
    else:
        physical_devices = tf.config.experimental.list_physical_devices('CPU')


    # initial parameters
    session = InteractiveSession(config=config)

    STRIDES, ANCHORS, NUM_CLASS, XYSCALE = utils.load_config(FLAGS)
    input_size = FLAGS.size
    image_path = FLAGS.image

    # 1) Load inputs
    images_data,original_image = load_images(image_path,input_size) 
    
    # 2) Load TFLite model and allocate tensors.
    interpreter = tf.lite.Interpreter(model_path=FLAGS.weights)
    interpreter.allocate_tensors()

    # 3) Get input and output tensors
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # 4) Run the inference
    interpreter.set_tensor(input_details[0]['index'], images_data)

    inferences_time = []

    for i in range(numb_infs):
        init_time = time.time()
        interpreter.invoke()
        final = time.time() - init_time
        inferences_time.append(final)
        
        fps = sum(inferences_time) / len(inferences_time)  
        total_time = sum(inferences_time)
        print(f" After {i} inferences required {total_time}, so {fps} FPS ")
    
    
    #pred = [interpreter.get_tensor(output_details[i]['index']) for i in range(len(output_details))]   
    #pred_bbox = extract_result(pred,input_size)
    #show_image(original_image, pred_bbox)
    

if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass