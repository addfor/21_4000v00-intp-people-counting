class BoundingBox():
    def __init__(self,tl,w,h):
        tl = (round(tl[0]),round(tl[1]))
        w = round(w)
        h = round(h)

        self.tl = tl
        self.tr = (tl[0] + w ,tl[1])
        self.bl = (tl[0]     ,tl[1] + h)
        self.br = (tl[0] + w ,tl[1] + h)
        self.width  = w
        self.height = h 

    @property
    def area(self):
        return self.width * self.height

    def __str__(self) -> str:
        return f" Top left: {self.tl}\n Top right: {self.tr}\n Bottom left: {self.bl}\n Bottom right: {self.br} \n Width: {self.width} \n Height: {self.height}"
    
    def __repr__(self) -> str:
        return self.__str__

    def halves(self):
        self.tl         =   (self.tl[0]//2, self.tl[1]//2)
        self.tr         =   (self.tr[0]//2, self.tl[1]//2)
        self.bl         =   (self.bl[0]//2, self.bl[1]//2)
        self.br         =   (self.br[0]//2, self.br[1]//2)
        self.width      =   self.width//2
        self.height     =   self.height//2
        return self

    def to_darknet_format(self, total_width, total_height):
        #<x_center> <y_center> <width> <height> - float values relative to width and height of image
        xc = (self.tl[0] + (self.width // 2))  / total_width
        yc = (self.tl[1] + (self.height // 2)) / total_height
        width = self.width      / total_width
        height = self.height    / total_height
        return [xc,yc,width,height]

    

