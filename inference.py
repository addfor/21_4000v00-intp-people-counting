from tensorflow.compat.v1 import ConfigProto
import tensorflow as tf
import cv2
import numpy as np
import core.utils as utils
from core.yolov4 import filter_boxes
#from crop import Crop        



class Inference():
    def __init__(self, params):
        config = ConfigProto()
        self.params = params

        if params['USE_CUDA']:
            physical_devices = tf.config.experimental.list_physical_devices('GPU')
            config.gpu_options.allow_growth = True
        else:
            physical_devices = tf.config.experimental.list_physical_devices('CPU')
    
        self.input_size = params['input_size']
        self.STRIDES, self.ANCHORS, self.NUM_CLASS, self.XYSCALE = utils.load_config(params)
        self.weights = params['weights']
        self.interpreter = tf.lite.Interpreter(model_path=params['weights'])
        self.interpreter.allocate_tensors()
        self.input_details =  self.interpreter.get_input_details()
        self.output_details = self.interpreter.get_output_details()


    
    '''
    def filter_border_box(self,boxes, margin):
        out_boxes, out_scores, out_classes, num_boxes = boxes
        to_filter_out = []
        
        # analizzo la vicinanza col bordo di ogni BB
        for i in range(num_boxes[0]):
            coor = out_boxes[0][i]

            #se supera la soglia data butta via tutto
            if (min(coor) < margin[0]) or (max(coor) > margin[1]):
                to_filter_out.append(i)

        out_boxes   = np.delete(out_boxes, to_filter_out)
        out_scores  = np.delete(out_scores, to_filter_out)
        out_classes = np.delete(out_classes, to_filter_out)
        num_boxes  -= len(to_filter_out)
        
        boxes = [out_boxes, out_scores, out_classes, num_boxes] 
        return boxes
    '''


    # Extract results
    def extract_result(self,pred):
        
        #wrong
        #boxes     = pred[0]
        #pred_conf = pred[1]

        #right
        boxes, pred_conf = filter_boxes(pred[0], pred[1], score_threshold=0.25, input_shape=tf.constant([self.input_size, self.input_size]))
        
        boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
            boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
            scores=tf.reshape(
                pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
            max_output_size_per_class=50,
            max_total_size=50,
            iou_threshold=  self.params['iou'],
            score_threshold=self.params['score']
        )
        pred_bbox = [boxes.numpy(), scores.numpy(), classes.numpy(), valid_detections.numpy()]
        return pred_bbox

    # testare se si può ottimizzare
    # se non ha il batch aggiungerlo automaticamente
    def make_inference(self,image):
        if isinstance(image,str):
            image = self.load_image(image)

        image = self.preprocess_image(image) 
        
        if len(image.shape) == 3:
            image = np.expand_dims(image, axis=0)

        self.interpreter.set_tensor(self.input_details[0]['index'], image)
        self.interpreter.invoke()
        pred = [self.interpreter.get_tensor(self.output_details[i]['index']) for i in range(len(self.output_details))]   
        return pred
        
    def load_image(self,path):
        image = cv2.imread(path)
        return image

    # 1) Load, Preprocess the image to required size and cast
    def preprocess_image(self, image):  
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = cv2.resize(image, (self.input_size, self.input_size))
        image = image / 255.

        image = np.array(image).astype(np.float32)
        return image
