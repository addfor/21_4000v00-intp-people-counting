'''
import numpy as np

class Crop():
    
    # target_shape = automatic pad is added if 
    # this parameters is setted and the central 
    # image shape is lower than the provided target_shape
    def __init__(self, img, target_shape=None):
      print("target_shape: ", target_shape)
      self.target_shape = target_shape
      self.margin_up = None
      self.margin_down = None
      self.margin_left = None
      self.margin_right = None
      self.center = img
      self.set_img(img)

    def create_padding(self,h,w):
      return np.zeros((h, w, 3), dtype=np.uint8)

    def set_img(self,img):
      if not self.target_shape is None:
        # create padder with the target_shape dimensions 
        padded_img = self.create_padding(self.target_shape[0],self.target_shape[1])
        padded_img[0:img.shape[0],0:img.shape[1]] = img
        img = padded_img

      self.img = img 


    def set_top_margin(self,margin):
      self.margin_up = margin

      if not self.margin_left is None:
        filler = self.create_padding(self.margin_left.shape[1], margin.shape[0])
        margin = self.concat_left(margin, filler)

      if not self.margin_right is None:
        filler = self.create_padding(self.margin_right.shape[1], margin.shape[0])
        margin = self.concat_right(margin, filler)

      self.img = self.concat_up(self.img,margin)

    def set_bottom_margin(self,margin):
      self.margin_down = margin

      if not self.margin_left is None:
        filler = self.create_padding(self.margin_left.shape[1], margin.shape[0])
        margin = self.concat_left(margin, filler)

      if not self.margin_right is None:
        filler = self.create_padding(self.margin_right.shape[1], margin.shape[0])
        margin = self.concat_right(margin, filler)

      self.img = self.concat_down(self.img,margin)

    def set_left_margin(self,margin):
      self.margin_left = margin
      
      if not self.margin_up is None:
        filler = self.create_padding(self.margin_up.shape[0], margin.shape[1])
        margin = self.concat_up(margin, filler)

      if not self.margin_down is None:
        filler = self.create_padding(self.margin_down.shape[0], margin.shape[1])
        margin = self.concat_down(margin, filler)
      
      self.img = self.concat_left(self.img,margin)


    def set_right_margin(self,margin):
      self.margin_right = margin

      if not self.margin_up is None:
        filler = self.create_padding(self.margin_up.shape[0], margin.shape[1])
        margin = self.concat_up(margin, filler)

      if not self.margin_down is None:
        filler = self.create_padding(self.margin_down.shape[0], margin.shape[1])
        margin = self.concat_down(margin, filler)

      self.img = self.concat_right(self.img,margin)

    def concat_up(self,center,up):
      return np.row_stack((up,center))

    def concat_down(self,center,down):
      return np.row_stack((center,down))

    def concat_right(self,center,right):
      return np.column_stack((center,right))

    def concat_left(self,center,left):
      return np.column_stack((left,center))
'''