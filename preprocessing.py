from re import M
import cv2
import tensorflow as tf
import numpy as np
import json
import random
import utility as utils
from boundingBox import BoundingBox
from darknet.head_counting.test_yolo import test_show_img_darknet
from pathlib import Path
import math

class cropManager():

    def __init__(self,crop_size=832,seed=0):
        self.crop_size = crop_size
        self.seed = seed 
        if self.seed > 0:
            random.seed(self.seed) #26           

    
    def getRandomCropCoord(self,img):    
        if img.shape[0] <= self.crop_size:
            padd = (self.crop_size - img.shape[0]) + 16
            img = cv2.copyMakeBorder(img, padd//2, padd//2, 0, 0, cv2.BORDER_CONSTANT)

        if img.shape[1] <= self.crop_size:
            padd = (self.crop_size - img.shape[1]) + 16
            img = cv2.copyMakeBorder(img, 0, 0, padd//2, padd//2, cv2.BORDER_CONSTANT)

        x = random.randint(0, img.shape[1] - self.crop_size)
        y = random.randint(0, img.shape[0] - self.crop_size)
        img = img[y:y+self.crop_size, x:x+self.crop_size, :]
        randomCrop = BoundingBox((x,y),self.crop_size,self.crop_size)
        return randomCrop

    def apply_crop(self,img,crop):
        cropped_img = img[crop.tl[1]:crop.bl[1], crop.bl[0]:crop.br[0]] 
        return cropped_img
                

    def get_intersection(self,boxA, boxB):
        if (boxA.tl[0] < boxB.tl[0]) and (boxA.tr[0] < boxB.tl[0]):
            return None
                    
        if (boxB.tl[0] < boxA.tl[0]) and (boxB.tr[0] < boxA.tl[0]):
            return None
        
        if (boxA.bl[1] < boxB.bl[1]) and (boxA.bl[1] < boxB.tl[1]):
            return None

        if (boxB.bl[1] < boxA.bl[1]) and (boxB.bl[1] < boxA.tl[1]):
            return None

        # determine the (x, y)-coordinates of the intersection rectangle
        Ixr = min(boxA.tr[0],    boxB.tr[0])
        Iyb = min(boxA.bl[1],    boxB.bl[1])
        Ixl = max(boxA.tl[0],    boxB.tl[0])
        Iyu = max(boxA.tl[1],    boxB.tl[1])

        widht = max(0, Ixr - Ixl) 
        height = max(0, Iyb - Iyu)

        intersection = BoundingBox((Ixl,Iyu),widht,height)
        return intersection


    def bb_IoU(self,boxA, boxB):    
        intersection = self.get_intersection(boxA, boxB)

        if intersection:
            # compute the area of both the prediction and ground-truth
            # rectangles
            boxAArea = (boxA.tr[0] - boxA.tl[0]) * (boxA.br[1] - boxA.tr[1])
            boxBArea = (boxB.tr[0] - boxB.tl[0]) * (boxB.br[1] - boxB.tr[1])

            # compute intersection area
            interArea = intersection.area
            
            # compute the intersection over union by taking the intersection
            # area and dividing it by the sum of prediction + ground-truth
            # areas - the interesection area
            iou = interArea / float(boxAArea + boxBArea - interArea)

            assert 0.0 <= iou <= 1.0

            # return the intersection over union value
            return iou
        else:
            return 0.0

    def vertex_list_2_bb(self,vertices):
        tl = (vertices[0],vertices[1])
        w  = abs(vertices[0] - vertices[2])
        h  = abs(vertices[1] - vertices[3])
        bb = BoundingBox(tl,w,h)
        return bb

    def draw_all_bb(self,img,list_bb):
        for bb in list_bb:
            bb_obj = self.vertex_list_2_bb(bb)
            utils.draw_bb(img,[bb_obj])
        #cv2.imwrite("./test_cropped.jpg", copied_img)

    def filter_label(self,labels,crop):
        filterd_labels = [] 

        # Genera la lista di BB da mettere
        for bb in labels:
            bb_obj = self.vertex_list_2_bb(bb)
            intersection = self.get_intersection(crop,bb_obj)
            if intersection:
                overlapArea = intersection.area / bb_obj.area 
                assert 0 <= overlapArea <= 1, f"overlapArea: {overlapArea}"
            
                if overlapArea > 0.5:
                    relative_coord_int = self.calc_relative_coord(crop,intersection)
                    filterd_labels.append(relative_coord_int)

        return filterd_labels

    # NON USATA
    def multi_filter_label(self,bb,crop):
        # Genera la lista di BB da mettere
        bb_obj = self.vertex_list_2_bb(bb)
        intersection = self.get_intersection(crop,bb_obj)
        if intersection:
            overlapArea = intersection.area / bb_obj.area 
            assert 0 <= overlapArea <= 1, f"overlapArea: {overlapArea}"
        
            if overlapArea > 0.5:
                relative_coord_int = self.calc_relative_coord(crop,intersection)
                return relative_coord_int
        
        return None
    
    # calculate relative position of a subBox with rispect to the current box 
    def calc_relative_coord(self,bigBox,subBox):
        new_tl = (subBox.tl[0] - bigBox.tl[0], subBox.tl[1] - bigBox.tl[1])
        new_bb = BoundingBox(new_tl,subBox.width,subBox.height)
        return new_bb   
    
    def apllyResize(self,img,labels):
        labels = [l.halves() for l in labels]
        img = cv2.resize(img, (img.shape[0]//2,img.shape[1]//2))
        return img,labels


#####################################################################################################

if __name__ == '__main__':
    inp_imgs_path = Path("./dataset/nwpu/original/imgs")
    inp_labels_path = Path("./dataset/nwpu/original/detections")
    out_imgs_path = Path("./dataset/nwpu/cropped_padded/imgs")
    out_imgs_path.mkdir(parents=True, exist_ok=True)
    
    train_file = open("./darknet/head_counting/head_train.txt","a")
    val_file = open("./darknet/head_counting/head_valid.txt","a")
    
       
    crop_size = 832
    cm = cropManager(crop_size)
    cropForImage = 10
    dump = False
    
    for img_path in inp_imgs_path.glob("*.jpg"):
        print(img_path)

        # init path to load
        img_id = img_path.stem
        img_filename =  Path(img_id)
        ann_filename =  Path(img_id)  
         
        # load image and annotations
        image = cv2.imread(str(inp_imgs_path / img_filename.with_suffix(".jpg")))
        annotations = utils.load_label(str(inp_labels_path / ann_filename.with_suffix(".json")))
        annotations = annotations['boxes']

        if len(annotations) < 1000:
            for c in range(cropForImage):
                # apply random crop to the image
                crop_coord = cm.getRandomCropCoord(image)

                crop_img = cm.apply_crop(image,crop_coord)
                # apply random crop to the annotation
                filtered_labels = cm.filter_label(annotations,crop_coord)
                crop_img,filtered_labels = cm.apllyResize(crop_img,filtered_labels)
                #print(f"{img_id}_{c} has {len(filtered_labels)} filtered_labels: ")

                # dump cropped image
                cv2.imwrite(str(out_imgs_path / f"{img_filename}_{c}.jpg"), crop_img)

                # dump cropped annotation
                with open(f"{str(out_imgs_path)}/{img_id}_{c}.txt", 'w') as f:
                    for fl in filtered_labels:
                        xc, yc, box_w, box_h = fl.to_darknet_format(crop_img.shape[0],crop_img.shape[1])
                        text = f'0 {xc:.6f} {yc:.6f} {box_w:.6f} {box_h:.6f}\n'
                        f.write(text)

                filename = str(out_imgs_path / f"{img_filename}_{c}.jpg")
                if  3110 < int(img_id) < 3609:
                    val_file.write(f"{filename}\n")
                else:
                    train_file.write(f"{filename}\n")
            
    val_file.close()
    train_file.close()