import cv2

class Patch():
    def __init__(self, img, coordinates):
        '''
        coordinates         =   indicate the coordinates of the patch w.r.t the original images
        absolute_img_shape  =   absolute coordinates from the original outer image from wich the patch is extracted. 
                                It is necessary with the coordinates for calculating bordes
        
        borders = list that indicate in which border the patch is located
        '''
        self.coordinates = coordinates
        self.img = img

    @classmethod
    def calculate_borders(coordinates,absolute_img_shape):
        marg_left,marg_right,marg_up,marg_bottom = coordinates
        borders = []
        if marg_up == 0:
            borders.append("N")
        if marg_right == absolute_img_shape[0]:
            borders.append("S")
        if marg_left == 0:
            borders.append("W")
        if marg_right == absolute_img_shape[1]:
            borders.append("E")

        return borders

        



class SlidingWindow():
    def __init__(self,stride,patch_size):
        assert stride < patch_size[0]
        assert stride < patch_size[1]
        self.stride = stride 
        self.patch_size = patch_size 
    
    
    def pad_image(self,img):
        overflow_right  = self.stride - ((img.shape[1]-self.patch_size[1]) % self.stride)
        overflow_bottom = self.stride - ((img.shape[0]-self.patch_size[0]) % self.stride)
        return cv2.copyMakeBorder(img, 0, overflow_bottom, 0, overflow_right, cv2.BORDER_CONSTANT)

    def get_num_patches(self,img,patch_size=None,stride=None):
        if not patch_size is None:
            psize = patch_size
        else:
            psize = self.patch_size

        if not stride is None:
            strd = stride
        else:
            strd = self.stride
        
        tot_patches_rows = ( (img.shape[1]-psize[1]) // strd ) + 1
        tot_patches_cols = ( (img.shape[0]-psize[0]) // strd ) + 1

        totale_patches = tot_patches_rows * tot_patches_cols 
        return totale_patches

    
    def extract_patches(self,original_img):
        padded_img = self.pad_image(original_img)
        #padded_img = SlidingWindow.pad_image(original_img,self.patch_size,self.stride)
        counter = 0 
        for col in range(0,original_img.shape[0]-self.patch_size[0],self.stride):
            for row in range(0,original_img.shape[1]-self.patch_size[1],self.stride):
                marg_left = row 
                marg_right = marg_left + self.patch_size[0]  
                marg_up = col 
                marg_bottom = marg_up + self.patch_size[1]
                crop = padded_img[marg_up:marg_bottom,marg_left:marg_right]
                
                ## TO PYTEST
                assert crop.shape[0] == self.patch_size[0], f"because {crop.shape[0]} != {self.patch_size[0]}"
                assert crop.shape[1] == self.patch_size[1], f"because {crop.shape[1]} != {self.patch_size[1]}"
                counter += 1
                #totale_patches = self.get_num_patches(original_img)
                #assert totale_patches == counter, f"totale_patches != counter, totale_patches = {totale_patches} and counter = {counter}"
                ####
                
                yield crop, [marg_left,marg_right,marg_up,marg_bottom]
                
                
        