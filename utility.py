import cv2
import tensorflow as tf
import numpy as np
import json

def load_label(path):
    with open(path) as f:
        label = json.load(f)
        human_num   = label['human_num']
        boxes       = label['boxes']
        points      = label['points']
        assert human_num == len(boxes), "Different number of human_num e num_boxes"
        
        label = {
            "human_num" : human_num,
            "boxes"     : boxes,
            "points"    : points,
        }

        return label
        

def draw_bb(image,boxes,points=None, color=(0,0,255)):

    for id_det in range(len(boxes)):

        tl = boxes[id_det].tl
        br = boxes[id_det].br

        if points:
            center = (round(points[id_det][0]),round(points[id_det][1]))
            cv2.circle(image,center, radius=0, color=color, thickness=20)

        cv2.rectangle(image,tl,br,color,5)


def show_image(img):
    img = np.array(img)
    cv2.imshow("image", img)
    cv2.waitKey(0)
