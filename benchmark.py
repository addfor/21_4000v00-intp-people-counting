import multiprocessing
from functools import partial
import time
import cv2
import tensorflow as tf
import numpy as np
import json
import random
import utility as utils
from boundingBox import BoundingBox
from preprocessing import cropManager

MULTI = True

total_bench = 2

img_id = "0004" #(3072, 4884, 3)
ds_path = "./test_dataset/"
labels = utils.load_label(f"{ds_path}label/{img_id}.json")
image = cv2.imread(f"{ds_path}imgs/{img_id}.jpg")

random_crop_time = 0
filter_time = 0
total_time = 0 
global_time = 0
apply_crop_time = 0

for i in range(total_bench):
    print("Tentativo: ", i)
    image_test = image.copy()
    cm = cropManager()
    
    global_init_time = time.time()
    
    init_time = time.time()
    crop_coord = cm.getRandomCropCoord(image_test)
    random_crop_time += time.time() - init_time
    
    init_apply_crop_time = time.time()
    crop_img = cm.apply_crop(image_test,crop_coord)
    apply_crop_time += time.time() - init_apply_crop_time

    init_time = time.time()
    
    if MULTI: 
        # Versione multi thread
        #filtered_labels = [cm.multi_filter_label(l,crop_coord) for l in labels['boxes']]
        # using filter() to remove None values in list
        #filtered_labels = list(filter(None, filtered_labels))

        with multiprocessing.Pool(processes=10) as pool:
            filtered_labels = pool.map(partial(cm.multi_filter_label, crop=crop_coord), labels['boxes'])
        # using filter() to remove None values in list
        #filtered_labels = list(filter(None, filtered_labels))
        filter_time += time.time() - init_time

    else:
        # Versione single thread
        filtered_labels = cm.filter_label(labels['boxes'],crop_coord)
        for fl in filtered_labels:
            utils.draw_bb(crop_img,[fl])
        
    filter_time += time.time() - init_time
    global_time += time.time() - global_init_time

    for fl in filtered_labels:
        if fl:
            utils.draw_bb(crop_img,[fl])

apply_crop_time /= total_bench
global_time /= total_bench
random_crop_time /= total_bench
filter_time /= total_bench

print("MULTI : ", MULTI)
print(f"Benchmarked over {total_bench} esperiments:")
print(f"Time for random crop: {random_crop_time}")
print(f"Time for Fitlering: {filter_time}")
print(f"Time for applying crop {apply_crop_time}")
print(f"General time {global_time}")


cv2.imwrite("./test_cropped.jpg", crop_img)