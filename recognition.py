class Rectangle():
    def __init__(self, box):
        self.y1_perc = box[0]  # percentuage coordinare 
        self.x1_perc = box[1] 
        self.y2_perc = box[2] 
        self.x2_perc = box[3] 
        self.perc_tl = (self.x1_perc,self.y1_perc)
        self.perc_br = (self.x2_perc,self.y2_perc)

    def get_perc_coord(self):
        return (self.perc_tl,self.perc_br)

    def get_abs_coords(self,img_shape):
        y1 = int(self.x1_perc * img_shape[0])
        x1 = int(self.y1_perc * img_shape[0])
        y2 = int(self.x2_perc * img_shape[1])
        x2 = int(self.y2_perc * img_shape[1])

        self.abs_tl = (x1,y1) 
        self.abs_br = (x2,y2)
                
        return (self.abs_tl, self.abs_br)
        

        
class Recognition():
    def __init__(self, boxes, pred_conf, classes, img_shape=None):
        if img_shape:
            self.boxes = Rectangle(boxes).get_abs_coords(img_shape) 
        else:
            self.boxes = Rectangle(boxes).get_perc_coord(img_shape) 

        self.pred_conf = pred_conf
        self.classes = classes
        self.img_shape = img_shape