import re
from tensorflow.compat.v1 import ConfigProto
import tensorflow as tf
import cv2
import numpy as np
#from absl.flags import FLAGS
#from absl import app, flags
import core.utils as utils
from core.yolov4 import filter_boxes
from inference import Inference
from pathlib import Path
from preprocessing import cropManager
import yaml
import tqdm


params = {
        "USE_CUDA"      : True,
        'tiny'  :   False,    
        'model' :   'yolov4',   
        'output'    : 'result.png',   
        'framework' : 'tf',   
        'weights'   : './checkpoints/yolov4-416.tflite',  
        'size'      : 416,  
        'image'     : './data/kite.jpg', 
        'iou'       : 0.45,    
        'input_size'    : 416,
        'score'     : 0.10,    
}

def extract_counter(img_path):
    img_path = Path(img_path)
    right_counter = int(img_path.stem.split("_")[-1]) 
    return str(img_path),right_counter


def stream_paths():
    base_path = Path("./dataset/Simo_Testnet/PER_LUCA/testset_masked/")
    for img_folder_path in base_path.iterdir():
        for img_path in img_folder_path.iterdir():
            if not img_path.stem.startswith("empty"):
                yield extract_counter(img_path)
        break



'''
def main():  
    
    base_out_path = Path("./benchmark")

    #possible_patches = [(2080,2080), (1664,1664), (1248,1248), (832,832), (416,416)]
    #possible_scores = [0.20,0.30,0.35,0.40,0.45,0.5,0.6]
    possible_patches = [(2080,2080)]
    possible_scores = [0.20,0.30]
    summary = dict()
    best_config = {'total_errors' : 1000}


    for patch_size in tqdm.tqdm(possible_patches):
        print(f"Using {patch_size=}")
        patch_out_path = base_out_path / Path(f"patch_{patch_size[0]}")
    
        for score in possible_scores:
            total_errors = []
            total_ground_true = []
            result_run = dict()
            
            print(f"Using {score=}")
            params['score'] = score

            out_path = patch_out_path / Path(f"score_{params['score']}")
            out_path.mkdir(parents=True, exist_ok=True)
            print("out_path: ",out_path)
        
            
            inf = Inference(params)
            for path_img,ground_counter in stream_paths():
                path_img = Path(path_img)
                predicted_count = 0
                image_data = inf.load_image(str(path_img))
                patches = inf.create_patches(image_data,patch_size)

                for id_patch in range(len(patches)):      
                    pred = inf.make_inference(patches[id_patch])
                    pred_boxes = inf.extract_result(pred)
                    #bounded_image = utils.draw_bbox(patches[id_patch], pred_boxes)
                    predicted_count += pred_boxes[3][0]

                #save for metrics
                total_ground_true.append(ground_counter)
                total_errors.append(abs(ground_counter - predicted_count))
                print(f"For {path_img} \n True counter: {ground_counter}, Prediction counter: {predicted_count}, errors: {total_errors[-1]}")   
                
                #dump image
                predicted_single_image = inf.recreate_image_from_patches(image_data.shape, patches)
                cv2.imwrite(f"{str(out_path)}/{path_img.name}",predicted_single_image)
            
            
            print(f"Computed {sum(total_errors)} total erros on a total of {sum(total_ground_true)} ground true")
            result_run['score'] = params['score'] 
            result_run['patch_size'] = patch_size
            result_run['total_errors'] = int(sum(total_errors))
            result_run['total_ground_true'] = sum(total_ground_true)
            config_to_dump = out_path / Path("conf.yml")

            with open(str(config_to_dump), 'w') as outfile:
                yaml.dump(result_run, outfile, default_flow_style=False)

            if result_run['total_errors'] <= best_config['total_errors']:
                best_config = result_run 

            summary[f"{patch_size[0]}_{score}"] = int(sum(total_errors))
            summary["best_config"] = best_config

        with open("./best_config.yml", 'w') as outfile:
            yaml.dump(best_config, outfile, default_flow_style=False)

    with open("./summary.yml", 'w') as outfile:
        yaml.dump(summary, outfile, default_flow_style=False)
    
'''
    

'''
NUOVA VERSIONE 
patch_size = (416,416)
inf = Inference(params)
stride = 100
Sw = SlidingWindow(stride,patch_size)
recognition = []
img_path = Path("./dataset/Simo_Testnet/PER_LUCA/testset_tomask/Cam12AIona/aula2_1_cnt_44.jpg")
image_data = cv2.imread(str(img_path))
counter = 0


for crop,coordinates in Sw.extract_patches(image_data):
    counter +=1
    pred = inf.make_inference(crop)
    pred_boxes = inf.extract_result(pred)
    boxes, pred_conf, classes, valid_detections = pred_boxes
    
    print("valid_detections[0]: ", valid_detections[0])

    boxes       = boxes[:,0:valid_detections[0],:]
    pred_conf   = pred_conf[:,0:valid_detections[0]] 
    classes     = classes[:,0:valid_detections[0]] 
    paint_img = crop.copy()

    for i in range(valid_detections[0]):
        r = Recognition(boxes[0][i], pred_conf[0][i], classes[0][i], crop.shape)
        recognition.append(r)
        tl,br = r.boxes
        draw_bb(paint_img,tl,br)

    plt.imshow(paint_img)  
    plt.show()
    plt.clf()
'''
    
def preprocess_patch():
    #patch_size = (2080,2080)
    patch_size = (416,416)
    score = 0.30
    margin_limit_filter = (0.10,0.90)
    out_path = Path("./test_dataset/test_preprocessing")
    img_path = Path("./dataset/Simo_Testnet/PER_LUCA/testset_masked/Cam11AIona/aula1_1_cnt_26.jpg")
            
    inf = Inference(params)
    path_img,ground_counter = extract_counter(img_path)
    path_img = Path(path_img)
    predicted_count = 0
    image_data = inf.load_image(str(path_img))
    patches = inf.create_patches(image_data,patch_size)

    #'''
    for id_patch in range(len(patches)):      
        pred = inf.make_inference(patches[id_patch])
        pred_boxes = inf.extract_result(pred)
        #pred_boxes = inf.filter_border_box(pred_boxes,margin_limit_filter) CAPIRE PERCHE' QUESTO METODO NON FUNZIONA E COMUNQUE ESPANDERLO CON IL NUOVO PREPROCESSING
        bounded_image = utils.draw_bbox(patches[id_patch], pred_boxes)
        predicted_count += pred_boxes[3][0]
        #out_patch_name = Path(f"{id_patch}_{path_img.name}")
        #cv2.imwrite(str(out_path/out_patch_name) ,bounded_image)
        cv2.imwrite(f"{str(out_path)}/patches_{id_patch}.jpg",patches[id_patch])
    #'''

    #dump image
    predicted_single_image = inf.recreate_image_from_patches(image_data.shape, patches)
    cv2.imwrite(f"{str(out_path)}/{path_img.name}",predicted_single_image)


if __name__ == '__main__':
    preprocess_patch()
    #main()