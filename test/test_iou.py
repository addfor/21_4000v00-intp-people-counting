#from mia_repo.utils import draw_bb, show_image
from mia_repo.boundingBox import BoundingBox
from mia_repo.preprocessing import bb_IoU,get_intersection
import numpy as np

def test_correct_IoU():
    
    #####################################################################
    #print("\n TEST 1 \n ")
    
    #generate img
    blank_image = np.zeros((300,300,3), np.uint8)
    
    #generate random BBs
    boxA = BoundingBox((0,0),100,50)
    boxB = BoundingBox((10,10),100,50)
    #draw_bb(blank_image,[boxA,boxB])
    #show_image(blank_image)
        
    iou = bb_IoU(boxA, boxB)
    assert iou >= 0.5
    assert bb_IoU(boxA, boxB) == bb_IoU(boxB, boxA), "Area is not simmetric"
    intersection = get_intersection(boxA, boxB)

    if intersection:

        #print("Area: ", intersection.area, intersection.width, intersection.height)
        #draw_bb(blank_image,[intersection],color=(255,100,100))
        #show_image(blank_image)
        assert intersection.area == 3600
        assert intersection.bl == (10,50)
        assert intersection.tl == (10,10)
        assert intersection.tr == (100,10)
        assert intersection.br == (100,50)
    
def test_horizontal_shifted_IoU():
    #####################################################################
    #print("\n TEST 2 \n ")
    
    #generate img
    blank_image = np.zeros((300,300,3), np.uint8)
    
    #generate random BBs
    boxA = BoundingBox((0,0),100,50)
    boxB = BoundingBox((200,0),100,50)
    #draw_bb(blank_image,[boxA,boxB])
    #show_image(blank_image)
  
  
    iou = bb_IoU(boxA, boxB)
    assert iou == 0.
    assert bb_IoU(boxA, boxB) == bb_IoU(boxB, boxA), "Area is not simmetric"
    intersection = get_intersection(boxA, boxB)

    if intersection:
    
        #draw_bb(blank_image,[intersection],color=(255,100,100))
        #show_image(blank_image)
        
        #print("Area: ", intersection.area, intersection.width, intersection.height)
        assert intersection.area == 0
        assert intersection.tl == (0,0)
        assert intersection.tr == (0,0)
        assert intersection.bl == (0,0)
        assert intersection.br == (0,0)

def test_vertical_shifted_IoU():
    #####################################################################
    #print("\n TEST 3 \n ")
    
    #generate img
    blank_image = np.zeros((300,300,3), np.uint8)
    
    #generate random BBs
    boxA = BoundingBox((0,0),100,50)
    boxB = BoundingBox((0,200),100,50)
    #draw_bb(blank_image,[boxA,boxB])
    #show_image(blank_image)
    
    iou = bb_IoU(boxA, boxB)
    assert iou == 0.
    assert bb_IoU(boxA, boxB) == bb_IoU(boxB, boxA), "Area is not simmetric"
    intersection = get_intersection(boxA, boxB)

    if intersection:
    
        #draw_bb(blank_image,[intersection],color=(255,100,100))
        #show_image(blank_image)
        
        #print("Area: ", intersection.area, intersection.width, intersection.height)
        assert intersection.area == 0
        assert intersection.tl == (0,0)
        assert intersection.tr == (0,0)
        assert intersection.bl == (0,0)
        assert intersection.br == (0,0)

def test_inside_IoU():
    #generate img
    blank_image = np.zeros((300,300,3), np.uint8)
    
    #generate random BBs
    boxA = BoundingBox((0,0),250,250)
    boxB = BoundingBox((50,50),10,10)
    #utils.draw_bb(blank_image,[boxA,boxB])
    #utils.show_image(blank_image)
        
    iou = bb_IoU(boxA, boxB)
    assert iou >= 0
    print("iou: ", iou)
    assert bb_IoU(boxA, boxB) == bb_IoU(boxB, boxA), "Area is not simmetric"
    intersection = get_intersection(boxA, boxB)

    if intersection:

        print("Area: ", intersection.area, intersection.width, intersection.height)
        #utils.draw_bb(blank_image,[intersection],color=(255,100,100))
        #utils.show_image(blank_image)
        print(intersection)
        assert intersection.area == 100

def test_overlap_IoU():
     #generate img
    blank_image = np.zeros((300,300,3), np.uint8)
    
    #generate random BBs
    boxA = BoundingBox((0,0),100,100)
    boxB = BoundingBox((0,0),100,100)
    #utils.draw_bb(blank_image,[boxA,boxB])
    #utils.show_image(blank_image)
        
    iou = bb_IoU(boxA, boxB)
    assert iou == 1
    assert bb_IoU(boxA, boxB) == bb_IoU(boxB, boxA), "Area is not simmetric"
    intersection = get_intersection(boxA, boxB)

    if intersection:

        print("Area: ", intersection.area, intersection.width, intersection.height)
        #utils.draw_bb(blank_image,[intersection],color=(255,100,100))
        #utils.show_image(blank_image)
        print(intersection)
        assert intersection.area == 10000
'''
def test_random_bb_IoU():
    for i in range(10):       
        #generate img
        blank_image = np.zeros((300,300,3), np.uint8)
        
        #generate random BBs
        boxA = BoundingBox((0,30),100,100)
        boxB = getRandomCropCoord(blank_image, 100, 100)
        #boxB = BoundingBox((50,10),100,100)
        utils.draw_bb(blank_image,[boxA,boxB])
        utils.show_image(blank_image)
            
        iou = bb_IoU(boxA, boxB)
        print("iou: ", iou)
        if iou > 0.5:
            print("Promosso")
        else:
            print("Bocciato")
            
        assert bb_IoU(boxA, boxB) == bb_IoU(boxB, boxA), "Area is not simmetric"
        intersection = get_intersection(boxA, boxB)

        if intersection:

            print("Area: ", intersection.area, intersection.width, intersection.height)
            utils.draw_bb(blank_image,[intersection],color=(255,100,100))
            utils.show_image(blank_image)
            print(intersection)
'''